//
//  CurrencyRates.swift
//  CurrencyConvertor
//
//  Created by Slavena on 4/22/17.
//  Copyright © 2017 Slavena. All rights reserved.
//

import Foundation

class CurrencyRates{
    
    var base: String
    var rates: [String: Float]
    
    init(base: String, rates: [String: Float]) {
        self.base = base
        self.rates = rates
    }
    
}
