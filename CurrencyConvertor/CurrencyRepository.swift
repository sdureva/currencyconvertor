//
//  CurrencyRepository.swift
//  CurrencyConvertor
//
//  Created by Slavena on 4/21/17.
//  Copyright © 2017 Slavena. All rights reserved.
//

import Foundation
import Alamofire

final class CurrencyRepository{
    private init() { }
    
    static let sharedInstance : CurrencyRepository = CurrencyRepository()
    
    
    func GetData(currency: String) -> NSDictionary?{
        performRequest(.get, requestURL: "https://api.fixer.io/latest?base=" + currency, params: [:], completion: { response in
            return response
        })
        return nil
    }
    
    func performRequest(_ method: HTTPMethod, requestURL: String, params: [String: AnyObject], completion: @escaping (_ json: AnyObject?) -> Void) {
        
        Alamofire.request(requestURL, method: .get, parameters: params, headers: nil).responseJSON { (response:DataResponse<Any>) in
            switch(response.result) {
            case .success(_):
                if response.result.value != nil{
                    let JSON = response.result.value as! NSDictionary
                    print(JSON.object(forKey: "rates") ?? "error getting dictionary from result")
                    completion(JSON)
                }
                break
                
            case .failure(_):
                print(response.result.error ?? "some error occured")
                completion(nil)
                break
                
            }
        }
    }
}

