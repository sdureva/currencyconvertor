//
//  CurrencyResultTableViewCell.swift
//  CurrencyConvertor
//
//  Created by Slavena on 4/22/17.
//  Copyright © 2017 Slavena. All rights reserved.
//

import Foundation
import UIKit

class CurrencyResultTableViewCell: UITableViewCell{
    
    @IBOutlet weak var flagImageView: UIImageView!
    @IBOutlet weak var currencyNameLabel: UILabel!
    @IBOutlet weak var resultLabel: UILabel!
    @IBAction func swichTapped(_ sender: Any) {
        let notificationName = Notification.Name("switchTapped")
        NotificationCenter.default.post(name: notificationName, object: currencyNameLabel.text)
    }
}
