//
//  ViewController.swift
//  CurrencyConvertor
//
//  Created by Slavena on 4/21/17.
//  Copyright © 2017 Slavena. All rights reserved.
//

import UIKit

class CurrencyMainViewController: UIViewController, UITableViewDelegate, UITableViewDataSource {

    @IBOutlet weak var tableView: UITableView!
    @IBAction func textFieldEditingDidEnd(_ sender: Any) {
        self.tableView.reloadData()
    }
    @IBOutlet weak var inputTextField: UITextField!
    @IBOutlet weak var currencyButton: UIButton!
    
    //var currencies = ["GBP", "DKK", "RUB", "EUR", "USD", "CZK", "CHF"]
    
    var currencies : [CurrencyRates] = []
    var bgnRates: CurrencyRates = CurrencyRates(base: "BGN", rates: ["GBP" : 0.42806, "DKK": 3.8025, "RUB": 30.809, "EUR": 0.5113, "USD": 0.54699, "CZK": 13.771, "CHF": 0.5460700000000001])
    var eurRates: CurrencyRates = CurrencyRates(base: "EUR", rates: ["BGN": 1.9558, "GBP" : 0.42806, "DKK": 3.8025, "RUB": 30.809, "USD": 0.54699, "CZK": 26.933, "CHF": 0.5460700000000001])
    
    var czkRates: CurrencyRates = CurrencyRates(base: "CZK", rates: ["BGN": 0.072617, "GBP" : 0.42806, "DKK": 3.8025, "RUB": 30.809, "EUR": 0.037129, "USD": 0.54699, "CHF": 0.5460700000000001])
    
    var baseCurrency = "BGN"
    
    var currentRate: CurrencyRates?
    
    override func viewDidLoad() {
        super.viewDidLoad()

        tableView.delegate = self
        tableView.dataSource = self
        
        let tap: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(CurrencyMainViewController.dismissKeyboard))
        inputTextField.addTarget(self, action: #selector(textFieldDidChange(_:)), for: .editingChanged)
        self.view.addGestureRecognizer(tap)
        let result = CurrencyRepository.sharedInstance.GetData(currency: "BGN")
        print(result ?? "no result")
        
        let notificationName = Notification.Name("switchTapped")
        NotificationCenter.default.addObserver(self, selector: #selector(changeBase), name: notificationName, object: nil)
        
        currencies.append(eurRates)
        currencies.append(czkRates)
        currentRate = bgnRates
    }
    
    func changeBase(notification: Notification){
        let newBase = notification.object as! String
        self.baseCurrency = newBase
        
        if let index = currencies.index(where: { $0.base == newBase }) {
            let currencyToRemove = currencies[index]
            currencies.insert(currentRate!, at: 0)
            //currencies.append(currentRate!) //prepend, we want the lastest first followed by the most used
            currentRate = currencyToRemove
            let indexOfCurrencyToRemove = currencies.index{ $0.base == currencyToRemove.base}
            if let i = indexOfCurrencyToRemove{
                currencies.remove(at: i)
            }
        }
        currencyButton.titleLabel?.text = newBase
        tableView.reloadData()
    }
    
    func textFieldDidChange(_ textField: UITextField) {
        tableView.reloadData()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 40.0
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return currencies.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell: CurrencyResultTableViewCell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath) as! CurrencyResultTableViewCell
        let currencyCode: String = currencies[indexPath.row].base
        let currencyRate: Float = currentRate!.rates[currencyCode]!
        var result: Float = 0.0
        if let num = Float(inputTextField.text!){
            result = num * currencyRate
        }
        cell.currencyNameLabel.text = currencyCode
        cell.resultLabel.text = String(result)
        return cell
    }
    
    func dismissKeyboard(){
        self.inputTextField.resignFirstResponder()
    }

}

